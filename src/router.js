import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

export default new Router({
  base: process.env.BASE_URL,
  routes: [{
    path: '/',
    name: 'back',
    component: ()=>import('./zhenzhou/back.vue'),
    children:[{
      path: '/main',
      name: 'main',
      icon:'',
      component: ()=>import('./zhenzhou/main.vue'),
    },{
      path: '/energy',
      name: 'energy',
      component: ()=>import('./zhenzhou/energy.vue')
    }]
  }]
})
