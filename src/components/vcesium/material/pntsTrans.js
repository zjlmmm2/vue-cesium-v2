export var xyzToCart2=function(pnts){
    let arr2=[]
    for(let i=0;i<pnts.length/3;i++){
        arr2.push(pnts[3*i],pnts[3*i+1])
    }
    return arr2
}
export var xyzToCart3=function(pnts,hei=100,close=false){
    let arr3=[]
    for(let i=0;i<pnts.length/3;i++){
        arr3.push(pnts[3*i],pnts[3*i+1],hei)
    }
    if(close){
        arr3.push(pnts[0],pnts[1],hei)
    }
    return arr3
}

export var xyToCart3=function(pnts,hei=100,close=false){
    let arr3=[]
    for(let i=0;i<pnts.length/2;i++){
        arr3.push(pnts[2*i],pnts[2*i+1],hei)
    }
    if(close){
        arr3.push(pnts[0],pnts[1],hei)
    }
    return arr3
}