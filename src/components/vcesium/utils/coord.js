// import * as Cesium from 'cesium'
let Cesium = require('cesium/Cesium')
export var coord = function () {}
coord.Cartesian2llh = function (viewer, cartesian3) {
	var ellipsoid = viewer.scene.globe.ellipsoid
	var cartographic = ellipsoid.cartesianToCartographic(cartesian3)
	return {
		lon: Cesium.Math.toDegrees(cartographic.longitude),
		lat: Cesium.Math.toDegrees(cartographic.latitude),
		hei: cartographic.height,
	}
}
