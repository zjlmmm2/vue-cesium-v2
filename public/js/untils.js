// 小边界
var smallploy2 = [
   [118.123805,27.294259],
[118.123226,27.294402],
[118.122657,27.294921],
[118.122214,27.295733],
[118.121986,27.296545],
[118.122638,27.296746],
[118.121601,27.297533],
[118.121362,27.301321],
[118.120393,27.303468],
[118.118591,27.303602],
[118.117035,27.303011],
[118.115989,27.301733],
[118.115105,27.301495],
[118.114071,27.3016],
[118.110076,27.299207],
[118.107918,27.29638],
[118.101511,27.293477],
[118.099687,27.293859],
[118.099419,27.293706],
[118.100618,27.291018],
[118.102074,27.288176],
[118.104539,27.284915],
[118.106918,27.282646],
[118.110545,27.28543],
[118.110748,27.285259],
[118.112653,27.286593],
[118.11457,27.286536],
[118.114692,27.286956],
[118.120495,27.288443],
[118.121534,27.289092],
[118.121641,27.288944],
[118.122283,27.289196],
[118.121178,27.290956],
[118.123762,27.291723],
]
// 大边界
var bigploy2 = [
    [118.125929,27.290216],
[118.125929,27.293239],
[118.128118,27.294979],
[118.126208,27.295696],
[118.126755,27.295902],
[118.123853,27.298713],
[118.123646,27.301834],
[118.121723,27.305337],
[118.118569,27.305699],
[118.115683,27.304527],
[118.115249,27.303502],
[118.11413,27.303561],
[118.108507,27.300579],
[118.106726,27.297504],
[118.101364,27.295608],
[118.099349,27.295847],
[118.097104,27.294624],
[118.098664,27.290847],
[118.100996,27.287222],
[118.104193,27.282722],
[118.106886,27.280395],
[118.11048,27.283066],
[118.110781,27.282741],
[118.114042,27.284849],
[118.117025,27.284853],
[118.117228,27.285885],
[118.121922,27.286592],
[118.1256,27.288757],
[118.124513,27.289969],
]
var polygonArr2 = [
    {
        name: '停车场',
        point: [
            118.11672,27.30145,
            118.116183,27.302003,
            118.11717,27.302442,
        ],
    },{
        name: '污水处理厂',
        point: [
            118.119653,27.290722,
            118.119234,27.290769,
            118.118226,27.290559,
            118.118463,27.290064,
            118.119986,27.290465,
        ]
    }
]

// 有序所需点位
var pointArr2 = [
    {
        name: "应急池",
        point: [118.122925,27.289619],
        srcUrl: '',
    },{
        name: "消防站",
        point: [118.11172,27.30145],
        srcUrl: 'static/theme/images/mapPoint/xiaofangzhan.png',
    },{
        name: "医疗救援中心",
        point: [118.11372,27.30145],
        srcUrl: 'static/theme/images/mapPoint/yiliaojiuyuandui.png',
    },{
        name: "封闭化管理",
        point: [118.11472,27.30145],
        srcUrl: '',
    },
]
var arr2to3=function(arr2,hei=0){
    let arr3=[]
    for(let i=0;i<arr2.length;i++){
        arr3.push(arr2[i][0],arr2[i][1],hei)
    }
    arr3.push(arr2[0][0],arr2[0][1],hei)
    return arr3
}
var smallploy3=arr2to3(smallploy2,10)
var bigploy3=arr2to3(bigploy2,12)
var arrbaiduto3=function(arr2,hei=10){
    let arr3=[]
    for(let i=0;i<arr2.length;i++){
        arr3.push(arr2[i].lng,arr2[i].lat,hei)
    }
    arr3.push(arr2[0].lng,arr2[0].lat,hei)
    return arr3
}
var keepArr=function(arr,n=0){
    if(arr.length>n){
        arr.splice(n,arr.length-n)
    }
}