1 npm方式安装 cesium>1.64
2 拷贝 src/components/vcesium 到项目中可直接使用
3 引用结构
cesiumContainer()
    c_layer_ArcGisMap
    cLayer
    c_pop_billboards(:arrda='billboards')
    c_tile_b3dm()
4 控件分类说明
    父控件 src/components/vcesium/viewer/cesiumContainer.vue
    其他所有为子控件
    4.1 底图
        路径 /vcesium/imageryLayers
    4.2 b3dm模型
        路径 /vcesium/tiles
        输入 url: 模型路径
            aboveH: 模型抬高高度
    4.3 基本几何体
        路径 /vcesium/tech
        techwall
        polygon 
    4.4 热力图 
        路径 /vcesium/tech/heatmap
        需要在 index.html中引入 heatmap.js插件
        输入 点位gps坐标 [{lon:120,lat:38}] 计算热力图
    5.5 billboard弹窗回显
        路径 /vcesium/popViewer
        需要手动添加.vue弹窗页面,并且在父控件中添加注册类
    5.6 打点工具(需要完善)
        路径 /vcesium/drawtool
5 后续扩展
    整个vcesium可直接替换
